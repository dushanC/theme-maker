<?php

// Set variables for our request
$shop = $_GET['shop'];
$api_key = "d696bd6db0736395e46036f9a7b984d1";
$scopes = "write_themes,read_themes";
$redirect_uri = "https://7fa7-89-205-127-248.ngrok-free.app/generate_token.php";

// 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
// Build install/approval URL to redirect to
$install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . $redirect_uri;
// https://probavaradin.myshopify.com/admin/oauth/authorize?client_id=b1977c9d4676f10eae41341822f7403c&scope=write_themes,read_themes&redirect_uri=https%3A%2F%2F7fa7-89-205-127-248.ngrok-free.app%2Fgenerate_token.php
// Redirect
header("Location: " . $install_url);
die();

