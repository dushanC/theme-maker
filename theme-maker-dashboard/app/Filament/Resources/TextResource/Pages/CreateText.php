<?php

namespace App\Filament\Resources\TextResource\Pages;

use App\Filament\Resources\TextResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateText extends CreateRecord
{
    protected static string $resource = TextResource::class;
}
